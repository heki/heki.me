// Copyright (C) 2023  Heki
//
// This file is part of heki.me.
//
// heki.me is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// heki.me is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with heki.me.  If not, see <https://www.gnu.org/licenses/>.

use std::{fmt, io, path::PathBuf, time::Duration};

use actix_http::{body::MessageBody, Request, Response};
use actix_service::{IntoServiceFactory, ServiceFactory};
use actix_web::{dev::AppConfig, Error, HttpServer};
use clap::Parser;
use serde::Deserialize;

#[derive(Parser)]
pub struct Arguments {
    /// Path to the config
    #[arg(short, long)]
    pub config: Option<PathBuf>,
}

#[derive(Clone, Deserialize)]
pub struct Actix {
    hosts: Vec<String>,
    num_workers: Option<usize>,
    backlog: Option<u32>,
    max_connections: Option<usize>,
    max_connection_rate: Option<usize>,
    keep_alive: Option<u64>,
    client_timeout: Option<u64>,
    client_shutdown: Option<u64>,
    shutdown_timeout: Option<u64>,
    pub compression: Option<bool>,
    pub normalize_path: Option<bool>,
}

#[derive(Clone, Deserialize)]
pub struct Settings {
    pub actix: Actix
}

impl Settings {
    pub fn parse_from_toml(path: PathBuf) -> Result<Self, String> {
        match std::fs::read_to_string(path) {
            Ok(toml_str) => {
                let parsed: Result<Self, toml::de::Error> = toml::from_str(&toml_str);
                match parsed {
                    Ok(c) => Ok(c),
                    Err(e) => Err(format!("Error in config: {}", e)),
                }
            }
            Err(error) => {
                return Err(format!("Error opening config file: {}", error.to_string()));
            }
        }
    }

    pub fn apply<F, I, S, B>(
        &self,
        mut server: HttpServer<F, I, S, B>,
    ) -> Result<HttpServer<F, I, S, B>, io::Error>
    where
        F: Fn() -> I + Send + Clone + 'static,
        I: IntoServiceFactory<S, Request>,
        S: ServiceFactory<Request, Config = AppConfig> + 'static,
        S::Error: Into<Error>,
        S::InitError: fmt::Debug,
        S::Response: Into<Response<B>>,
        B: MessageBody + 'static,
    {
        for host in &self.actix.hosts {
            server = server.bind(host)?;
        }

        if let Some(value) = &self.actix.num_workers {
            server = server.workers(*value);
        }

        if let Some(value) = &self.actix.backlog {
            server = server.backlog(*value)
        }

        if let Some(value) = &self.actix.max_connections {
            server = server.max_connections(*value)
        }

        if let Some(value) = &self.actix.max_connection_rate {
            server = server.max_connection_rate(*value)
        }

        if let Some(value) = &self.actix.keep_alive {
            server = server.keep_alive(Duration::from_secs(*value))
        }

        if let Some(value) = &self.actix.client_timeout {
            server = server.client_request_timeout(Duration::from_millis(*value))
        }

        if let Some(value) = &self.actix.client_shutdown {
            server = server.client_disconnect_timeout(Duration::from_millis(*value))
        }

        if let Some(value) = &self.actix.shutdown_timeout {
            server = server.shutdown_timeout(*value)
        }

        Ok(server)
    }
}
