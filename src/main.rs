// Copyright (C) 2023  Heki
//
// This file is part of heki.me.
//
// heki.me is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// heki.me is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with heki.me.  If not, see <https://www.gnu.org/licenses/>.

use actix_files::Files;
use actix_web::{
    get, http::header::ContentType, middleware::{Compress, Logger, NormalizePath, TrailingSlash}, web::{self, Redirect}, App, HttpResponse, HttpServer, Responder
};
use clap::Parser;
use heki::config::{Arguments, Settings};
use tera::{Context, Tera};

#[get("/")]
async fn root(tera: web::Data<Tera>) -> impl Responder {
    let context = Context::new();
    let render = tera.render("index.html", &context).unwrap();
    
    HttpResponse::Ok().content_type(ContentType::html()).body(render)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));

    let arguments = Arguments::parse();
    let config_file = arguments.config.unwrap_or("config.toml".into());
    let settings = Settings::parse_from_toml(config_file).unwrap();

    let tera = Tera::new("templates/**/*.html").unwrap();

    let server = HttpServer::new(move || {
        App::new()
            .wrap(Compress::default())
            .wrap(NormalizePath::new(TrailingSlash::Trim))
            .wrap(Logger::default())
            .app_data(web::Data::new(tera.clone()))
            .service(Redirect::new("/sourcecode", "https://gitlab.com/heki/heki.me"))
            .service(Redirect::new("/license", "https://www.gnu.org/licenses/agpl-3.0"))
            .service(Redirect::new("/gitlab", "https://gitlab.com/heki"))
            .service(Redirect::new("/github", "https://github.com/linuxheki"))
            .service(root)
            .service(Files::new("/", "static"))
    });

    settings
        .apply(server)
        .unwrap()
        .run()
        .await
}
